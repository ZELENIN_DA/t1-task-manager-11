# TASK MANAGER

## DEVELOPER INFO

**NAME**: Daniil Zelenin

**EMAIL**: dzelenin@t1-consulting.ru

**EMAIL**: zelenin4ever@gmail.com

## SOFTWARE

**JAVA**: OPENKDK 1.8

**OS**: WINDOWS 10 x64

## HARDWARE

**CPU**: i5-8250U

**RAM**: 12GB

**SSD**: 500GB

## BUILD PROGRAM

```
mvn clean install
```

## RUN PROGRAM

```
java -jar ./task-manager.jar
```
