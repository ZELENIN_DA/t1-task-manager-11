package ru.t1.dzelenin.tm.api;

public interface ITaskController {

    void createTask();

    void showTask();

    void clearTask();

    void removeTaskById();

    void removeTaskByIndex();

    void showTaskById();

    void showTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();
}
